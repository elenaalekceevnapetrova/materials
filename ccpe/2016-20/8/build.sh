rm *.log
rm *.aux
rm *.out
rm *.synctex.gz

mkdir pdf
for i in *.tex; do
	lualatex --output-directory pdf "$i"
	lualatex --output-directory pdf "$i"
done

cd pdf
rm *.log
rm *.aux
rm *.out
rm *.synctex.gz
