mkdir pdf

rm *.log
rm *.aux
rm *.out
rm *.synctex.gz

for i in *.tex; do
	pdflatex --output-directory pdf "$i"
	pdflatex --output-directory pdf "$i"
done

cd pdf
rm *.log
rm *.aux
rm *.out
rm *.synctex.gz
